
## Repository for Micro Crystal RV 3028 RTC driver for Nordic NRF52 family

----

[[_TOC_]]

----


## Introduction

The following document is the complete firmware lifecycle document for RTC Driver . It is divided into 4 sections.

1.	Requirements Specifications
2.	Detailed Design Document
3.	Test Plan
4.	Additional Documentation

The Software Requirements Specifications (SRS) lists the purpose of the step and the various requirements that the driver must satisfy. It also lists any conventions that the requirements specifications must adhere to.

The Detailed Design Document (DDD) stage describes the overall hardware and firmware architecture. The DDD describes the constraints, prerequisites and conventions that need to be followed / satisfied. Lastly, DDD also identifies the individual modules and their public interfaces.

The Tests document are a complementary supporting document to the SRS and the DDD. The Tests document specify a measurable, validatable method to verify that a module or feature works as expected to. There are three stages of Tests that the document will cover.

  Unit Tests : These tests are performed at a module / class level. They test only the Module / Class under test, as a blackbox.

  Integration tests : These tests verify the interaction between one module / class and the rest of the system. These are used to verify the state transitions and handling functionality.

  Regression Tests : These tests are added later to implement a feature after completion or bug fixes. They are used to verify that any code change later does not break the existing functionality.

This document is limited to specifying Unit Tests and Integration tests. The test plan must also identify and map a valid requirement to test(s). 

## Intended audience

The entire document is intended to be used by the maintainers and developers of the firmware. It is intended to be version controlled and updated to reflect present state of the firmware. It is to be seen as a tool for identifying the requirements, design methodologies and constraints and the design / implementation choices. Any design choice for tool, framework, library, methodology must be justified in the following Designed Document.

----

## SRS

### Introduction

#### Purpose

The purpose of the SRS is to :-

1. Describe the product functionality
2. Specify the operating conditions
3. Specify the functional requirements of the firmware
4. Give a priority to each requirement
5. Specify the available interfaces and describe the stimulus / responses of the system.
6. Specify the non functional and safety requirements


#### Document Conventions

##### Requirements numbering

All the functional and non functional requirements that are identified must be numbered from "R001" and incremented. If a requirement needs to be referred elsewhere in the document, it must be done so by linking to the specific requirement using the Requirement number.

##### Priority Levels

MoSCoW<sup>[1](#references)</sup> method will be used to prioritize the requirements.

Any requirement can have one and only one of the following status
1.  Must Have   ( MH )
2.  Should Have ( SH )
3.  Could Have  ( CH )
4.  Wont Have   ( WH )

In decreasing order of priority. Every requirement must have the abbreviations ( in **bold** ) at the end of the line.


#### Scope

The scope of the SRS as explained in the introduction is limited to primarily identifying the various features, functions and constraints of the driver. The SRS does not and shall not identify any specific tool, choice, methodology that needs to be used / implemented. Further any of the following requirements shall strive to remain agnostic of the implementation specifics. They must and should only identify the behavior of the system without touching any underlying details.

#### References

1. [MoSCoW Method](https://en.wikipedia.org/wiki/MoSCoW_method)

### Assumptions
1.  The driver is running on a bare metal firmware without any RTOS handling it.  
2.  If there is an RTOS running, there is a mechanism to prevent access to the same I2C peripheral at the same time when the RTC is operating.  

3.  The log messages are optional and can be enabled or disabled using a defined symbol.  

4.  The time parameter written to / read from the RTC is of the datatype time_t type. 

### Interfaces

#### Hardware Interfaces  

1. I2C 


### Functional Requirements
| Status | Req No | Priority | Requirement | 
|----|----|----|----|
| &cross; | R001 | MH | The driver must have a function to initialize the RTC |
| &check; | R002 | MH | The driver must have a function to set the time |
| &check; | R003 | MH | The driver must have a function to get the time |
| &check; | R004 | MH | The time is specified in UNIX epoch time format for both read and write |
| &check; | R005 | SH | The driver must have a function to get date as a null terminated string |
| &check; | R006 | SH | The driver must have a function to get day as a null terminated string |
| &check; | R007 | SH | The driver must have a function to get time as a null terminated string |
| &check; | R008 | SH | The driver must return error if the inputs are NULL or out of bounds |
| &cross; | R009 | WH | The driver must have a function to set an alarm, specified in seconds |  

### Non Functional Requirements

1.  The driver should be stable and reliable - i.e predictable, deterministic operation and none to low failure of operation.
2.  The driver should be designed to allow for scalability - allow for different kinds of RTC and communication interfaces.
3.  The driver must be as modular and decoupled as possible.
4.  The driver must use the nrf_log module to log information.


### Safety Requirements

1.  Any failure mode of operation should be handled gracefully and safely without any risk to user or equipment.
2. Error Handling ( Critical Errors ) - Any critical errors should result in termination of program execution and the driver must return an error code that must be propogated to the calling function.
3. Error Handling ( Non Critical Errors )- In case of non critical errors, the firmware should log the error and continue operation.

### Constraints

1.  The driver must fit within the flash and memory constraints of the NRF52840 processor.
2.  The driver must be written in pure C.

----

## Detailed Design Document
### Architecture

### Introduction
The following Detailed Design Document attempts to create a design specification for the firmware driver development. The section attempts to specify the dependancies / prerequisites for the development (hardware, firmware, software) in order to have a reproducible environment. Further, the section also lists the constraints under which the firmware must operate. The constraints can be hardware ( voltage, operating conditions, low power, memory and flash restrictions) or firmware ( necessary libraries, power management apparoaches, peripheral restrictions and limitations). The document may contain a basic hardware architecture diagram to indicate the connection of the RTC to the development board and what physical and logical interfaces are required. This section maybe split into several diagrams to enhance clarity. The document shall use the [C4 Model ](https://en.wikipedia.org/wiki/C4_model) to arrive at  different diagrams to identify the exact boundaries and interfaces between different layers and modules in the driver. Given the relative complexity of the driver, not all diagrams may be present in the final driver.


### Environment and Components
The following section lists the development environment (hardware and software) and the components ( toolchain, libraries, build system) used or intended to be used. Further, it also briefly touches what are minimum hardware tools that might aid in debugging during the development of the firmware or for troubleshooting.

#### Hardware 
1.  NRF52-DK development board
2.  Micro Crystal RV-3028 RTC demo board
3.  Jumper wires.

#### Software ( Development Environment)
1.  OS - MacOS Big Sur
2.  IDE - Segger Embedded Studio with NRF SDK support 

#### Software ( Firmware Environment & Libraries)
1.  Toolchain - GNU ARM GCC (Segger Embedded Studio)

2.  HAL - NRF SDK

3.  Unit Test Framework - [Ceedling](https://github.com/ThrowTheSwitch/Ceedling)

    Ceedling:: 0.31.0

    Unity:: 2.5.2

    CMock:: 2.5.3

    CException:: 1.3.3

4.  COTS Libraries -

5.  Debugging - Segger Embedded Studio

#### Version Control

The firmware must be version controlled and use git as a VCS. In order to standardize and streamline development and commit messages, the firmware development must follow [Git-flow](https://github.com/nvie/gitflow) for feature development and [Commitizen](https://github.com/commitizen-tools/commitizen) utility for commiting the changes. Further, the release versioning must follow 
[SemVer](https://semver.org) standard. Git-Flow tooling is SemVer compliant.

### Document Conventions

#### Coding Standards

The firmware must strive to adhere to the [GNU C Coding Style](https://www.gnu.org/prep/standards/html_node/Writing-C.html) as far as possible.
Special care must be taken for naming conventions, indentation, spacing and commenting. A brief set of rules that need to be followed are given below

1.  Tab = 4 spaces.
2.  80 character column width.
3.  Naming style must be CamelCase.
4.  Naming convention must be [AppsHungarian](https://en.wikipedia.org/wiki/Hungarian_notation). The prefix must indicate the logical datatype rather than the actual datatype.
5.  Any function must not exceed one screen length between braces.
6.  Interfaces must try and adhere to C file I/O conventions - Read, Write, Init, Open, Close, IO etc.
7. Use [mutator methods](https://en.wikipedia.org/wiki/Mutator_method) to set or get values from datastructures or other interfaces.
8. Code must be commented using doxygen. There must be a description block in the header before every function / datastructure / macro / definition section.

### Architecture diagrams

#### Hardware architecture block diagram
```plantuml
@startmindmap
scale max 900 width
caption Hardware Connection

+ NRF52-DK Board
++ RV-3028 breakoutboard
@endmindmap
```

#### Sequence diagram

##### Sequence Diagram for writing time to the RTC

```plantuml
@startuml
scale max 900 width
caption Sequence diagram to write time to the RTC

Power_up ->  2:  Init NRF I2C Peripheral
2 ->  3:  Initialize RTC
3 ->  4:  Write Time to the RTC

```


##### Sequence Diagram for reading time from the RTC

```plantuml
@startuml
scale max 900 width
caption Sequence diagram to write time to the RTC

Power_up ->  2:  Init NRF I2C Peripheral
2 ->  3:  Initialize RTC
3 ->  4:  Read time from the RTC

```


#### Software architecture block diagram of driver


```plantuml
@startuml

top to bottom direction
skinparam dpi 600
scale max 900 width
circle app

Interface RTC
Interface RV_3028
Interface Log
Interface nrf_log
Interface NRFSDK
Interface I2Cdriver

app -> RTC
app -> Log

RTC --> RV_3028
RV_3028 --> I2Cdriver
I2Cdriver --> NRFSDK

Log --> nrf_log

Interface RTC
RTC : enum Error_codes RTCInit()
RTC : enum Error_codes RTCWriteEpochTime(time_t *pTime)
RTC : enum Error_codes RTCReadEpochTime(time_t *pTime)
RTC : enum Error_codes RTCGetDate(char* pDate)
RTC : enum Error_codes RTCGetDay(char* pDay)
RTC : enum Error_codes RTCGetTime(char* pTime)
RTC : enum Error_codes RTCDeInit()

Interface RV_3028
RV_3028 : enum Error_codes RV3028Init()
RV_3028 : enum Error_codes RV3028SetEpochTime(int32_t *pTime)
RV_3028 : enum Error_codes RV3028GetEpochTime(int32_t *pTime)

Interface I2Cdriver
I2Cdriver : enum Error_codes i2CSendStart(void)
I2Cdriver : enum Error_codes i2CRecvByte(char RxByte)
I2Cdriver : enum Error_codes i2CSendByte(char TxByte)
I2Cdriver : enum Error_codes i2CSendStop(void)

@enduml
```

#####  Notes  
1.  The functions <ins>underlined</ins> in the diagrams above are static functions, visible only to the source file.  
2.  1B, 1C, 1D, 1E addressess for the Unix Time counter for the RTC - also places a implicit requirement on the size of the datatype for time_t = 32 bit since there are only 4, 8 bit registers.  
3.  Look into Alarm functions - based on seconds converted into a future time.  
4.  ID register (28h) can be used as a proxy for initializing the RTC. 
5.  Need functions for sending i2c start, stop, send acknowledgement, read acknowledgement, send byte, receive byte on the LL NrF SDK HAL.  
6.  Add tests to check for the correctness of the data returned in RTCGetDay, RTCGetTime and RTCGetDate.  
7.  Use gmtime, not localtime to convert time_t to a struct tm type.  
8.  Add tests to check for the correct function calls to lower level modules.


### Layers, Interfaces and Member Functions

### Typedefs, enums, defines and  structures

```C
enum Error_codes {
  SUCCESS = 0,
  ERROR,
  RV3028_INIT_FAIL,
  RTC_INIT_FAIL,
  NULL_ARG_ERROR,
  ARG_OUTOFBOUNDS_ERROR,
  RV3028_READ_TIME_ERROR,
  RTC_READ_TIME_ERROR,
  ARG_SIZE_ERROR,
  ERRORCODES_END
};

```
```C
#define OVF_32BIT 4294967296 //32 bit overflow size
#define DATE_SIZE 9 // minimum size of char array to hold date
#define DAY_SIZE 10 // minimum size char array to hold day
#define TIME_SIZE 9 // minimum size char array to hold time
#define RV3028_ADD 0b1010010 //RV3028 7 bit I2C address
#define ETIME_START_ADD 0x1B //32 bit Unix counter address within RV3028
#define ID_REG_ADD 0x28 //8 bit ID register address within RV3028

```

### Error codes
Any error code in the system must be classified into two types of errors :-

1.  Critical Errors
2.  Non Critical Errors

For a critical error, the driver must gracefully suspend all operations and return the appropriate error code to the calling function.

For a non critical error, the driver can continue operation but still return the appropriate error to indicate loss of normal operation.

The following are the errors that must be addressed by the firmware.

**Critical**


**Non-Critical**


## Test Plan  
The following section outlines the plan to test the RTC driver module. The driver is split into 2 layers - the RTC Object and the RV3028 peripheral module. The RTC Object "sits" on top of the RV3028 peripheral driver and which itself sits on top of the NRFSDK HAL.

The goal is to test :-  
1.  Interfaces i.e parameters and return values.
2.  Behavior - i.e whether the appropriate lower level function is being called.
3.  Exception Handling - i.e whether any errors are appropriately propogated.

The test cases are all numbered, starting from T001 and incrementing upwards. Unlike requirements, there is no prioritization of test cases since all the tests need to be implemented in order to validate the driver. The actual tests themselves must have the test number as a part of the function name to indicate which test they are actually checking for.

### Test Cases  

|S No | Test |
|----|----|
|T001|  RTCInit must return an error if the RV3028Init fails.  |
|T002|  RTCWriteEpochTime must return an error if the argument is NULL  or out of bounds.  |
|T003|  RTCWriteEpochTime must call RV3028SetEpochTime with appropriate argument.  |
|T004|  RTCReadEpochTime must return an error if RV3028GetEpochTime returns and error or NULL.  |
|T005|  RTCReadEpochTime must return an error if argument is NULL  |
|T006|  RTCGetDate must return an error if argument is NULL.  
|T007|  RTCGetDate must return an error if sizeof argument is smaller than expected.  |
|T008|  RTCGetDay must return an error if argument is NULL.  
|T009|  RTCGetDay must return an error if sizeof argument is smaller than expected.  |
|T010|  RTCGetTime must return an error if argument is NULL.  
|T011|  RTCGetTime must return an error if sizeof argument is smaller than expected.  |
|T012| RV3028Init must return an error if the NRF52 cannot communicate with the RTC.  |
|T013| RV3028SetEpochTime must return an error if the argument is NULL or out of bounds.  |
|T014| RV3028GetEpochTime must return an error if the sizeof argument is smaller than expected or NULL  |
|T015| RTCGetDay returns correctly formatted day for a valid epoch time  |
|T016| RTCGetDate returns correctly formatted date for a valid epoch time  |
|T017| RTCGetTime returns correctly formatted time for a valid epoch time  |
|T018| RV3028SetEpochTime calls I2Cdriver functions with the correct arguments and in the correct sequence |
|T019| RV3028GetEpochTime calls I2Cdriver functions with the correct arguments and in the correct sequence |


Any additional test cases will be added as the development progresses.

The following table maps the requirements vs test cases as a matrix to indicate which tests address which requirement

||R001|R002|R003|R004|R005|R006|R007|R008|
|----|----|----|----|----|----|----|----|----|
|T001|&check;||||||||
|T002||&check;||&check;||||&check;|
|T003||&check;||||||&check;|
|T004|||&check;|&check;||||&check;|
|T005|||&check;|&check;||||&check;|
|T006|||||&check;|||&check;|
|T007|||||&check;|||&check;|
|T008||||||&check;||&check;|
|T009||||||&check;||&check;|
|T010|||||||&check;|&check;|
|T011|||||||&check;|&check;|
|T012|&check;||||||||
|T013||&check;||&check;||||&check;|
|T014|||&check;|&check;||||&check;|
|T015||||||&check;|||
|T016|||||&check;||||
|T017|||||||&check;||
|T018||&check;|||||||
|T019|||&check;||||||


## Additional Information

### Folder Structure
```C
.
├── Output
│   └── Debug
├── build
│   ├── artifacts
│   ├── logs
│   ├── temp
│   └── test
├── nRF
│   ├── Device
│   └── Scripts
├── src
├── test
│   └── support
└── vendor
    └── ceedling
```

1.  Output - Created by Segger Studio for build artefacts.  
2.  build - created by ceedling for test artefacts.  
3.  nRF - Created by Segger Studio. nRF specific files.  
4.  src - Actual file directory. All source and header files go here.  
5.  test - tests source files, creating modules using ceedling will create a pair of .c / .h files in the src folder and a corresponding test_\<module\>.c file in this folder.
6.  vendor  - ceedling files for docs and utilities.

### Segger Studio Instructions

1.  This folder can be imported directly into Segger Studio and run. However, a couple of change need to be made. In Segger Studio, in the "Project Items" pane to the left, under the "Source Files", we will need to add the existing source files manually. Right click on the "Source Files" and select the "Add Existing Files" option. Navigate to the "src" folder in the project root and select all the source and header files to add them to the project build.  

### Ceedling Test Suite Instructions  

1.  Simply run "ceedling test:all" from the command line from within the project folder.

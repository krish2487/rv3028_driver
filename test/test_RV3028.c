#ifdef TEST

#include "unity.h"

#include "RV3028.h"
#include "mock_i2cDriver.h"
#include "stdint.h"

void setUp( void )
{
}

void tearDown( void )
{
}

void test_T012_RV3028Init_returns_error_if_RTC_comms_fails( void )
{
  TEST_IGNORE_MESSAGE( "Need to Implement RV3028 Init Function" );
}

void test_T013_RV3028SetEpochTime_returns_error_on_NULL_argument( void )
{
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RV3028SetEpochTime( NULL ) );
  uint32_t* pTestTime = NULL;
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RV3028SetEpochTime( NULL ) );
}

void test_T014_RV3028GetEpochTime_returns_error_on_NULL_argument( void )
{
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RV3028ReadEpochTime( NULL ) );
}

void test_T018_RV3028SetEpochTime_calls_i2cDriver_functions_correctly( void )
{
  i2cSendStart_ExpectAndReturn( SUCCESS );
  i2cSendByte_ExpectAndReturn( 0xA4, SUCCESS );
  i2cSendByte_ExpectAndReturn( 0x1B, SUCCESS );
  i2cSendByte_ExpectAndReturn( 0x00, SUCCESS );
  i2cSendByte_IgnoreArg_TxByte( );
  i2cSendByte_ExpectAndReturn( 0x00, SUCCESS );
  i2cSendByte_IgnoreArg_TxByte( );
  i2cSendByte_ExpectAndReturn( 0x00, SUCCESS );
  i2cSendByte_IgnoreArg_TxByte( );
  i2cSendByte_ExpectAndReturn( 0x00, SUCCESS );
  i2cSendByte_IgnoreArg_TxByte( );
  i2cSendStop_ExpectAndReturn( SUCCESS );
  int32_t pTime = 1122334455;
  RV3028SetEpochTime( &pTime );
}

void test_T019_RV3028ReadEpochTime_calls_i2cDriver_functions_correctly( void )
{

  i2cSendStart_ExpectAndReturn( SUCCESS );
  i2cSendByte_ExpectAndReturn( 0xA4, SUCCESS );
  i2cSendByte_ExpectAndReturn( 0x1B, SUCCESS );
  i2cSendStop_ExpectAndReturn( SUCCESS );
  i2cSendStart_ExpectAndReturn( SUCCESS );
  i2cSendByte_ExpectAndReturn( 0xA5, SUCCESS );
  i2cRecvByte_ExpectAndReturn( 0x00, SUCCESS );
  i2cRecvByte_IgnoreArg_RxByte( );
  i2cRecvByte_ExpectAndReturn( 0x00, SUCCESS );
  i2cRecvByte_IgnoreArg_RxByte( );
  i2cRecvByte_ExpectAndReturn( 0x00, SUCCESS );
  i2cRecvByte_IgnoreArg_RxByte( );
  i2cRecvByte_ExpectAndReturn( 0x00, SUCCESS );
  i2cRecvByte_IgnoreArg_RxByte( );
  i2cSendStop_ExpectAndReturn( SUCCESS );
  int32_t pTime = 0;
  RV3028ReadEpochTime( &pTime );
}
#endif  // TEST

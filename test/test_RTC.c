#ifdef TEST

#include "unity.h"

#include "RTC.h"
#include "error.h"
#include "mock_RV3028.h"
#include "string.h"
void setUp( void )
{
}

void tearDown( void )
{
}

void test_RTC_T001_RTCInit_returns_error_if_RV3028_returns_error( void )
{
  RV3028Init_ExpectAndReturn( RV3028_INIT_FAIL );

  TEST_ASSERT_EQUAL( RTC_INIT_FAIL, RTCInit( ) );
}

void test_RTC_T001_RTCInit_returns_success_if_RV3028_returns_success( void )
{
  RV3028Init_ExpectAndReturn( SUCCESS );

  TEST_ASSERT_EQUAL( SUCCESS, RTCInit( ) );
}

void test_RTC_T002_RTCWriteEpochTime_returns_error_if_argument_is_null( void )
{

  time_t test_EpochTime = NULL;
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RTCWriteEpochTime( NULL ) );
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RTCWriteEpochTime( test_EpochTime ) );
}

void test_RTC_T002_RTCWriteEpochTime_returns_error_if_argument_is_out_bounds(
    void )
{

  // Overflow of the the 32 bit counter by one
  time_t test_EpochTime = 4294967296;
  TEST_ASSERT_EQUAL(
      ARG_OUTOFBOUNDS_ERROR, RTCWriteEpochTime( test_EpochTime ) );

  test_EpochTime = -1;
  TEST_ASSERT_EQUAL(
      ARG_OUTOFBOUNDS_ERROR, RTCWriteEpochTime( test_EpochTime ) );
}

void test_RTC_T003_RTCWriteEpochTime_calls_RV3028_with_appropriate_arg( void )
{
  time_t test_EpochTime = 1122334455;

  RV3028SetEpochTime_ExpectAndReturn( (int32_t*)test_EpochTime, SUCCESS );
  TEST_ASSERT_EQUAL( SUCCESS, RTCWriteEpochTime( test_EpochTime ) );
}

void test_RTC_T004_RTCReadEpochTime_returns_error_if_RV3028ReadEpochTime_returns_error(
    void )
{
  time_t test_EpochTime = 112233;
  RV3028ReadEpochTime_ExpectAndReturn( test_EpochTime, RV3028_READ_TIME_ERROR );
  TEST_ASSERT_EQUAL( RTC_READ_TIME_ERROR, RTCReadEpochTime( test_EpochTime ) );

  RV3028ReadEpochTime_ExpectAndReturn( test_EpochTime, NULL );
  TEST_ASSERT_EQUAL( RTC_READ_TIME_ERROR, RTCReadEpochTime( test_EpochTime ) );
}

void test_RTC_T005_RTCReadEpochTime_returns_error_if_argument_is_NULL( void )
{
  uint8_t test_EpochTime;
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RTCReadEpochTime( NULL ) );
}

void test_RTC_T006_RTCGetDate_returns_error_on_NULL_argument( void )
{
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RTCGetDate( NULL ) );
}

void test_RTC_T007_RTCGetDate_returns_error_on_incorrect_argument_size( void )
{
  char pDate[ 6 ];
  TEST_ASSERT_EQUAL( ARG_SIZE_ERROR, RTCGetDate( &pDate ) );
}

void test_RTC_T008_RTCGetDay_returns_error_on_NULL_argument( void )
{
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RTCGetDay( NULL ) );
}

void test_RTC_T009_RTCGetDay_returns_error_on_incorrect_argument_size( void )
{
  char pDay[ 6 ];
  TEST_ASSERT_EQUAL( ARG_SIZE_ERROR, RTCGetDay( &pDay ) );
}

void test_RTC_T010_RTCGetTime_returns_error_on_NULL_argument( void )
{
  TEST_ASSERT_EQUAL( NULL_ARG_ERROR, RTCGetTime( NULL ) );
}

void test_RTC_T011_RTCGetTime_returns_error_on_incorrect_argument_size( void )
{
  char pTime[ 6 ];
  TEST_ASSERT_EQUAL( ARG_SIZE_ERROR, RTCGetTime( &pTime ) );
}

void test_T015_RTCGetDay_returns_correct_day_if_rtc_time_is_valid( void )
{
  int32_t pTestTime = 1122334455;
  RV3028ReadEpochTime_ExpectAndReturn( &pTestTime, SUCCESS );
  RV3028ReadEpochTime_IgnoreArg_pTime( );
  RV3028ReadEpochTime_ReturnThruPtr_pTime( &pTestTime );

  char pDay[ 10 ];
  memset( pDay, '0', 10 );

  TEST_ASSERT_EQUAL( SUCCESS, RTCGetDay( &pDay ) );
  TEST_ASSERT_EQUAL_STRING( "Monday", pDay );
}

void test_T016_RTCGetDate_returns_correct_date_if_rtc_time_is_valid( void )
{
  int32_t pTestTime = 1122334455;
  RV3028ReadEpochTime_ExpectAndReturn( &pTestTime, SUCCESS );
  RV3028ReadEpochTime_IgnoreArg_pTime( );
  RV3028ReadEpochTime_ReturnThruPtr_pTime( &pTestTime );

  char pDate[ 10 ];
  memset( pDate, '0', 10 );

  TEST_ASSERT_EQUAL( SUCCESS, RTCGetDate( &pDate ) );
  TEST_ASSERT_EQUAL_STRING( "07/25/05", pDate );
}

void test_T017_RTCGetTime_returns_correct_time_if_rtc_time_is_valid( void )
{
  int32_t pTestTime = 1122334455;
  RV3028ReadEpochTime_ExpectAndReturn( &pTestTime, SUCCESS );
  RV3028ReadEpochTime_IgnoreArg_pTime( );
  RV3028ReadEpochTime_ReturnThruPtr_pTime( &pTestTime );

  char pTime[ 10 ];
  memset( pTime, '0', 10 );

  TEST_ASSERT_EQUAL( SUCCESS, RTCGetTime( &pTime ) );
  TEST_ASSERT_EQUAL_STRING( "23:34:15", pTime );
}
#endif  // TEST

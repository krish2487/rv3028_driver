#include "RTC.h"
#include "RV3028.h"
#include "string.h"
#include "time.h"
enum Error_codes RTCInit( void )
{
  if ( RV3028_INIT_FAIL == RV3028Init( ) ) {
	return RTC_INIT_FAIL;
  }

  return SUCCESS;
}

enum Error_codes RTCWriteEpochTime( time_t epochTime )
{
  if ( NULL == epochTime ) {
	return NULL_ARG_ERROR;
  }

  if ( OVF_32BIT < epochTime || epochTime < 0 ) {
	return ARG_OUTOFBOUNDS_ERROR;
  }

  enum Error_codes rc;
  rc = RV3028SetEpochTime( (int32_t*)epochTime );
  if ( SUCCESS != rc ) {
	return rc;
  }

  return SUCCESS;
}

enum Error_codes RTCReadEpochTime( time_t epochTime )
{

  if ( NULL == epochTime ) {
	return NULL_ARG_ERROR;
  }

  enum Error_codes rc;

  rc = RV3028ReadEpochTime( (int32_t*)epochTime );

  if ( RV3028_READ_TIME_ERROR == rc || NULL == rc ) {
	return RTC_READ_TIME_ERROR;
  }

  return SUCCESS;
}

enum Error_codes RTCGetDate( char* pDate )
{
  if ( NULL == pDate ) {
	return NULL_ARG_ERROR;
  }

  if ( strlen( pDate ) < DATE_SIZE ) {
	return ARG_SIZE_ERROR;
  }
  int32_t pEpochTime;
  RV3028ReadEpochTime( &pEpochTime );
  time_t pBuffer = (time_t)pEpochTime;
  struct tm rtcCalendarTime;
  rtcCalendarTime = *gmtime( &pBuffer );
  strftime( pDate, strlen( pDate ), "%x", &rtcCalendarTime );
  return SUCCESS;
}

enum Error_codes RTCGetDay( char* pDay )
{

  if ( NULL == pDay ) {
	return NULL_ARG_ERROR;
  }

  if ( strlen( pDay ) < DAY_SIZE ) {
	return ARG_SIZE_ERROR;
  }

  int32_t pEpochTime;
  RV3028ReadEpochTime( &pEpochTime );
  time_t pBuffer = (time_t)pEpochTime;
  struct tm rtcCalendarTime;
  rtcCalendarTime = *gmtime( &pBuffer );
  strftime( pDay, strlen( pDay ), "%A", &rtcCalendarTime );

  return SUCCESS;
}

enum Error_codes RTCGetTime( char* pTime )
{
  if ( NULL == pTime ) {
	return NULL_ARG_ERROR;
  }

  if ( strlen( pTime ) < TIME_SIZE ) {
	return ARG_SIZE_ERROR;
  }

  int32_t pEpochTime;
  RV3028ReadEpochTime( &pEpochTime );
  time_t pBuffer = (time_t)pEpochTime;
  struct tm rtcCalendarTime;
  rtcCalendarTime = *gmtime( &pBuffer );
  strftime( pTime, strlen( pTime ), "%X", &rtcCalendarTime );
  return SUCCESS;
}
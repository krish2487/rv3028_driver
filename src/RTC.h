#ifndef RTC_H
#define RTC_H
#include "error.h"
#include "time.h"

/**
 * @brief       Initializes the RTC interface, by calling the driver specific
 * Initialization function
 *
 * @return      SUCCESS if The lower level initialization success or
 * RTC_INIT_FAIL otherwise
 */
enum Error_codes RTCInit( void );

/**
 * @brief       Writes the time, specified as time_t to the RTC.
 *
 * @param[in]  epochTime  The epoch time
 *
 * @return      SUCCESS, NULL_ARG_ERROR, ARG_OUTOFBOUNDS_ERROR
 */
enum Error_codes RTCWriteEpochTime( time_t epochTime );

/**
 * @brief       Reads the time , from the RTC.
 *
 * @param[in]  epochTime  The epoch time
 *
 * @return      SUCCESS, RTC_READ_TIME_ERROR
 */
enum Error_codes RTCReadEpochTime( time_t epochTime );

/**
 * @brief       Reads the date from the RTC
 *
 * @param      pDate  The date formatted as MM/DD/YY
 *
 * @return      Returns the date as a string.  The argument must be a minimum
of 9 characters long.
 */
enum Error_codes RTCGetDate( char* pDate );

/**
 * @brief      Reads the day from the RTC
 *
 * @param      pDay  The day
 *
 * @return      Returns the day as a string. The size of string must be atleast
 * 10 characters long.
 */
enum Error_codes RTCGetDay( char* pDay );

/**
 * @brief      Reads the time from the RTC
 *
 * @param      pTime  The time
 *
 * @return     Returns the time as a string. The size of the must be atleast 9
 * characters long
 */
enum Error_codes RTCGetTime( char* pTime );

#endif  // RTC_H

#ifndef RV3028_H
#define RV3028_H
#include "error.h"
#include "stdint.h"

/**
 * @brief      { function_description }
 *
 * @return      SUCCESS or ERROR
 */
enum Error_codes RV3028Init( void );

/**
 * @brief      { Sets the epoch time counter in the RV3028 }
 *
 * @param      pTime  The time
 *
 * @return      SUCCESS or ERROR
 */
enum Error_codes RV3028SetEpochTime( int32_t* pTime );

/**
 * @brief      { Reads the epoch time counter in the RV3028 }
 *
 * @param      pTime  The time
 *
 * @return      SUCCESS or ERROR
 */
enum Error_codes RV3028ReadEpochTime( int32_t* pTime );

#endif  // RV3028_H

#include "RV3028.h"
#include "i2cDriver.h"
#include "stddef.h"

enum Error_codes RV3028Init( void )
{

  return SUCCESS;
}

enum Error_codes RV3028SetEpochTime( int32_t* pTime )
{
  if ( NULL == pTime ) {
	return NULL_ARG_ERROR;
  }

  int32_t pTBuff = 1122334455;
  pTime          = &pTBuff;
  // Datasheet pg 93. sequence of write operation
  // 1.  Send Start Condition
  // 2.  Send SlaveAddress + Write bit = A4
  // 3.  Acknowledgement from RV3028
  // 4.  Send Register address 0x1B Epoch time register
  // 5.  Acknowledgement from RV3028
  // 6.  Send Least Significant Byte = (int32 Time)& 0xFF
  // 7.  Acknowledgement from RV3028
  // 8.  Send Second Least Significant Byte (int32 Time>>8)& 0xFF
  // 9.  Acknowledgement from RV3028
  // 10.  Send Third Least Significant Byte (int32 Time>>16)& 0xFF
  // 11.  Acknowledgement from RV3028
  // 12.  Send Most Significant Byte (int32 Time>>24)& 0xFF
  // 13.  Acknowledgement from RV3028
  // 14. Send Stop Condition
  i2cSendStart( );

  i2cSendByte( 0xA4 );

  // Read Acknowledge from RV3028

  i2cSendByte( 0x1B );
  // Read Acknowledge from RV3028

  i2cSendByte( ( *pTime & 0xFF ) );  // Adding '0' to int converts to char
  // Read Acknowledge from RV3028

  i2cSendByte( ( ( *pTime >> 8 ) & 0xFF ) );
  // Read Acknowledge from RV3028

  i2cSendByte( ( ( *pTime >> 16 ) & 0xFF ) );
  // Read Acknowledge from RV3028

  i2cSendByte( ( ( *pTime >> 24 ) & 0xFF ) );
  // Read Acknowledge from RV3028

  i2cSendStop( );

  return SUCCESS;
}

enum Error_codes RV3028ReadEpochTime( int32_t* pTime )
{
  if ( NULL == pTime ) {
	return NULL_ARG_ERROR;
  }

  // Datasheet pg 94. sequence of write operation
  // 1.  Send Start Condition
  // 2.  Send SlaveAddress + Write bit = A4
  // 3.  Acknowledgement from RV3028
  // 4.  Send Register address 0x1B Epoch time register
  // 5.  Acknowledgement from RV3028
  // 6.  Send Stop Condition
  // 7.  Send Start Condition.
  // 8.  Send Slave Address + Read bit = A5
  // 9.  Acknowledgement from RV3028
  // 10.  Read Least Significant Byte
  // 11.  Send Acknowledgement
  // 12.  Send Second Least Significant Byte
  // 13.  Send Acknowledgement
  // 14.  Send Third Least Significant Byte
  // 15.  Send Acknowledgement
  // 16.  Send Most Significant Byte
  // 17.  Send Acknowledgement
  // 18. Send Stop Condition

  char Byte0 = 0, Byte1 = 0, Byte2 = 0, Byte3 = 0;
  i2cSendStart( );

  i2cSendByte( 0xA4 );
  // Read Acknowledge from RV3028

  i2cSendByte( 0x1B );
  // Read Acknowledge from RV3028

  i2cSendStop( );

  i2cSendStart( );

  i2cSendByte( 0xA5 );
  // Read Acknowledge from RV3028

  i2cRecvByte( Byte0 );
  // Send Acknowledge to RV3028

  i2cRecvByte( Byte1 );
  // Send Acknowledge to RV3028

  i2cRecvByte( Byte2 );
  // Send Acknowledge to RV3028

  i2cRecvByte( Byte3 );
  // Send NotAcknowledge to RV3028

  i2cSendStop( );

  // Concatenate all the 4 bytes to one single 32 bit variable and store in
  // pTime variable.

  return SUCCESS;
}

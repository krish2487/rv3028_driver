#ifndef I2CDRIVER_H
#define I2CDRIVER_H
#include "error.h"
enum Error_codes i2cSendStart( void );
enum Error_codes i2cRecvByte( char RxByte );
enum Error_codes i2cSendByte( char TxByte );
enum Error_codes i2cSendStop( void );
#endif  // I2CDRIVER_H

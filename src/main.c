/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------

File    : main.c
Purpose : Generic application start

*/

#include "RTC.h"
#include "error.h"
#include <stdio.h>
#include <stdlib.h>

// Global Variables - use with caution.
enum Error_codes rc;

/*********************************************************************
 *
 *       main()
 *
 *  Function description
 *   Application entry point.
 */
int main( void )
{

  // NrF52 I2C peripheral Initialization code.

  rc = RTCInit( );
  if ( rc == RTC_INIT_FAIL ) {
	// Log error using log module.
  }

  do {

  } while ( 1 );
}

/*************************** End of file ****************************/

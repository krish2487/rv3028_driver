#ifndef ERROR_H
#define ERROR_H

#define OVF_32BIT 4294967296 - 1
#define DATE_SIZE 9
#define DAY_SIZE 10
#define TIME_SIZE 9
#define RV3028_ADD 0b1010010
#define ETIME_START_ADD 0x1B
#define ID_REG_ADD 0x28

/**
 * @brief      Return codes for the RTC and RV3028/3027 drivers. Only a single
 * enum is used to handle the errors for both the RTC interface and the lower
 * level RV3027/RV3028 devices since they are coupled. The error codes are
 * generic ( SUCCESS, ERROR) and specific to a device operation failure. The
 * specific errors will be added to the enum as the driver development
 * progresses.
 */
enum Error_codes {
  SUCCESS = 0,
  ERROR,
  RV3028_INIT_FAIL,
  RTC_INIT_FAIL,
  NULL_ARG_ERROR,
  ARG_OUTOFBOUNDS_ERROR,
  RV3028_READ_TIME_ERROR,
  RTC_READ_TIME_ERROR,
  ARG_SIZE_ERROR,
  ERRORCODES_END
};

#endif  // ERROR_H
